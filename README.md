# Salt Profiles Formula

A small formula that can be used to manage shell profile enhancements located
under `/etc/profile.d/*.sh`.

As the information contained in the profile files may vary according to
datacenter-specific information, this formula depends on the
*inventory-formula*.
