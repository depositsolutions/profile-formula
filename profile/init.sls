include:
  - inventory

{%- for file, contents in salt['pillar.get']('profile:shell', {}).items() %}

/etc/profile.d/{{ file }}.sh:
  file.managed:
    - name: /etc/profile.d/{{ file }}.sh
    - user: root
    - group: root
    - mode: 0755
    # We want it to be processed before any package is processed.
    - order: first
    - contents: |
        {{ contents | indent(8) }}
{% endfor %}
